
/**
 *
 * @author ryanmoujaled
 */
import java.util.Scanner;

public class GreatestCatch {
    
    private static int count = 0;
    
    public static void welcomeMessage() {
        
        System.out.println("Welcome.\n");
        System.out.println("Caution: Selecting any menu option besides the following with terminate the program and cause it to close for security reasons.\n");
        System.out.println("Menu:\n"
                           + "1. Enter Catch\n"
                           + "2. Display Onloading Catch Weights\n"
                           + "3. Display Catch Count\n"
                           + "4. Display Largest and Lowest Catch Weight\n"
                           + "5. Display Total Catch Weight\n"
                           + "6. Display Offloading Catch Weights\n"
                           + "7. Use device to count total\n"
                           + "8. Display Offloading Catch Weights\n"
                           + "9. Exit Program\n");
    }
    
    public static void enterCatch(int[] catchWeighter) {
        Scanner weightInput = new Scanner(System.in);
        int totalHaulWeight = 0;
        int newCatchWeight = 0;
        
        System.out.println("Please enter your catch value:");
        if (weightInput.hasNextInt() == true) {
            try {
                newCatchWeight = weightInput.nextInt();
            } catch (Exception e) {
                System.out.println("Option selected Not applicable.");
            }
            
            if (newCatchWeight >= 0 || newCatchWeight < 1001) {
                
                for (int i = 0; i < catchWeighter.length; i++) {
                    if (catchWeighter[i] == 0) {
                        
                        if (totalHaulWeight + newCatchWeight < 1001) {
                            catchWeighter[count] = newCatchWeight;
                            count++;
                            System.out.println("the count: " + count);
                        } else {
                            System.out.println("This much additional weight would capsize this trawler. Not Added.");
                        }
                        break;
                    }
                    
                    totalHaulWeight += catchWeighter[i];
                }
                
            } else {
                System.out.println("Cannot enter catch weights of less than 0 or greater than 1000.\n"
                                   + " One cantch weight of 1000 would make this trawler capsize\n"
                                   + "Dropped the catch - Moving compartments");
            }
        } else {
            System.out.println("Please enter only valid input");
        };
    }
    
    public static int updateCatchCount() {
        return count;
    }
    
    public static void displayCatchWeights(int[] catchWeighter, int haulCount) {
        
        System.out.println("\n The weights in the compartments: \n");
        for (int i = 0; i < haulCount; i++) {
            System.out.print(catchWeighter[i] + "    ");
        }
        System.out.println();
        
    }
    
    public static void displayOffloadingCatchWeights(int[] catchWeighter, int haulCount) {
        
        System.out.println("\n The offloading weights: \n");
        
        for (int i = haulCount - 1; i >= 0; i--) {
            System.out.print(catchWeighter[i] + "    ");
        }
        System.out.println();
        
    }
    
    public static void displayLargestAndSmallestCatch(int[] catchWeighter, int haulCount) {
        
        System.out.println("\n");
        int max = 0;
        int min = catchWeighter[0];
        
        for (int i = 0; i < haulCount; i++) {
            if (catchWeighter[i] > max) {
                max = catchWeighter[i];
            }
            if (catchWeighter[i] < min) {
                min = catchWeighter[i];
            }
        }
        System.out.println("The largest catch this session: " + max);
        System.out.println("The smallest catch this session: " + min);
    }
    
    public static int displayTotalWeight(int[] catchWeighter, int haulCount) {
        int totalHaulWeight = 0;
        
        for (int i = 0; i < haulCount; i++) {
            totalHaulWeight += catchWeighter[i];
        }
        System.out.println("\n");
        System.out.println("Total weight of hauls made today: " + totalHaulWeight);
        return totalHaulWeight;
    }
    
    public static void printHaulCount(int haulCount) {
        System.out.println("\n");
        System.out.println("Total number of catches today: " + haulCount);
    }
    
    public static void printAverageOfHaul(int[] catchWeighter, int haulCount) {
        int totalHaul = 0;
        double denominator = haulCount;
        double average = 0;
        
        for (int i = 0; i < haulCount; i++) {
            totalHaul += catchWeighter[i];
        }
        average = totalHaul / denominator;
        System.out.println("\n");
        System.out.println("The average weight of catches in the compartments are: " + average);
    }
    
    public static void summaryOfDay(int[] catchWeight, int numberOfHauls) {
        System.out.println("Returning to port!\n Summary of the day:");
        displayTotalWeight(catchWeight, numberOfHauls);
        printHaulCount(numberOfHauls);
        printAverageOfHaul(catchWeight, numberOfHauls);
        displayLargestAndSmallestCatch(catchWeight, numberOfHauls);
        displayCatchWeights(catchWeight, numberOfHauls);
        displayOffloadingCatchWeights(catchWeight, numberOfHauls);
    }
    
    /**
     * Here the program runs, main.
     */
    public static void main(String[] args) {
        
        int[] catchWeight = new int[100];
        int numberOfHauls = 0;
        int command = 0;
        welcomeMessage();
        
        Scanner input = new Scanner(System.in);
        
        boolean live = true;
        boolean overall = true;
        
        do {
            System.out.println("Please enter a menu number to choose an option.");
            try {
                command = input.nextInt();
            } catch (Exception e) {
                System.out.println("Input Not applicable. Please enter valid input only.\n Directing to catches in compartments.");
                command = 7;
                
            }
            
            if (command == 1) {
                int weight = displayTotalWeight(catchWeight, numberOfHauls);
                if (weight > 1000 || numberOfHauls == 100) {
                    System.out.println("Weight is greater than 1000kg or the number of hauls has reached a 100, returning to port \n"
                                       + "Enter any key to close the program");
                    command = 7;
                }
                enterCatch(catchWeight);
                numberOfHauls = updateCatchCount();
            }
            
            if (command == 2) {
                displayCatchWeights(catchWeight, numberOfHauls);
            }
            
            if (command == 3) {
                printHaulCount(numberOfHauls);
            }
            
            if (command == 4) {
                displayLargestAndSmallestCatch(catchWeight, numberOfHauls);
            }
            if (command == 5) {
                displayTotalWeight(catchWeight, numberOfHauls);
            }
            if (command == 6) {
                displayOffloadingCatchWeights(catchWeight, numberOfHauls);
            }
            if (command == 7) {
                System.out.println("Please weigh one fish and enter its weight:");
                double fishweight = 0;
                
                try {
                    fishweight = input.nextInt();
                } catch (Exception e) {
                    System.out.println("Please enter a valid number");
                };
                
                FishCounter.fishCount(catchWeight, fishweight, numberOfHauls);
            }
            if (command == 8) {
                System.out.println("Please weigh one fish and enter its weight:");
                double fishWeight = 0;
                int compartmentNumber = 0;
                
                try {
                    fishWeight = input.nextInt();
                } catch (Exception e) {
                    System.out.println("Please enter a valid number");
                };
                
                System.out.println("Please enter the compartment you want to check:");
                
                try {
                    compartmentNumber = 0;
                } catch (Exception e) {
                    System.out.println("Please enter a valid number");
                };
                
                FishCounter.compartmentFishCount(catchWeight, fishWeight, numberOfHauls, compartmentNumber);
            }
            if (command == 9) {
                summaryOfDay(catchWeight, numberOfHauls);
                
                live = false;
            }
            
        } while (live == true);
        
        
    }
    
}
