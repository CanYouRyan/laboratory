/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ryanmoujaled
 */
public class FishCounter {
    
    public static void fishCount(int[] compartment,double averageWeight, int numberOfHauls){
        
        double numberOfFishes = 0;
        int correctedCompartmentNumber = 1;
        for(int i=0; i<numberOfHauls; i++){
            numberOfFishes = Math.round((compartment[i]/averageWeight));
            System.out.println("In "+correctedCompartmentNumber+" there are "
                    +numberOfFishes+" fishes");
            correctedCompartmentNumber++;
        }
    }
    
    public static void compartmentFishCount(int[] compartment,double averageWeight,
            int numberOfHauls, int specificCompartment){
        
        System.out.println("Weight of this compartment: "+compartment[specificCompartment-1]);
        double numberOfFishes = Math.round((compartment[specificCompartment-1]/averageWeight));
        System.out.println("There are "+numberOfFishes+ " in this compartment");
        
    }
}
